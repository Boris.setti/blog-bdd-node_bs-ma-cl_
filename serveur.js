var express = require("express");
var mysql = require('mysql');
var bodyParser = require('body-parser');
const bcrypt = require('bcryptjs')
// var session = require('express-session')
var app = express();

const port = 8081;
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));
app.set('view engine', 'pug');

//////////////////verif login////////////////////////////////////

// app.get("/manipulation/afficherlist/:login/:mdp", function(req, res){
//     loginuser = req.params.login;
//     mdpuser = req.params.mdp;
//     con.query(` SELECT * FROM User WHERE username ="${loginuser}";`, (err, result, fields) => {
//         if (result.length < 1) {
//             res.json("login non exitente");
//         } else if (loginuser === result[0].username) {
//             let mdpverif = bcrypt.compareSync(mdpuser, result[0].password)
//             if (mdpverif == true) {





//                 res.json(`coucou`);
//             } else {
//                 res.json("mot de passe invalide")
//             }
//         } 
//     });
//     // res.json("coucou");
// }); 



////////////////////test/////////////////////////////////

app.get("/manipulation/modifpost/:login/:mdp/:idPost/:titre/:msg", function (req, res) {
    loginuser = req.params.login;
    mdpuser = req.params.mdp;
    idPost = req.param.idPost;
    titre = req.param.titre;
    msg = req.param.msg;
    console.log(loginuser);
    console.log(mdpuser);
    console.log(idPost);
    console.log(titre);
    console.log(msg);
});







/////////////////// connection to mysql /////////////////

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "Blog"
});
con.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");
});

/////////////validation inscription/////////////////////////
app.post('/inscription/ajout', function (req, res) {
    let login = req.body.login;
    let mdp = req.body.mdp;
    let email = req.body.email;
    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(mdp, salt);
    con.query(` SELECT * FROM User WHERE username ="${login}";`, (err, result, fields) => {
        if (result.length < 1) {
            con.query(`INSERT INTO User (username, email, password) VALUES ("${login}", "${email}", "${hash}");`);
            
            res.redirect('/');
        } else
        res.send("login existant")
    });
    
})

/////////////////validation connexion////////////////////


app.post('/validation/connexion', function (req, res) {
    let loginuser = req.body.login
    let mdpuser = req.body.mdp
    con.query(` SELECT * FROM User WHERE username ="${loginuser}";`, (err, result, fields) => {
        if (result.length < 1) {
            res.send("login non exitente");
        } else if (loginuser === result[0].username) {
            let mdpverif = bcrypt.compareSync(mdpuser, result[0].password)
            if (mdpverif == true) {
                res.redirect(`/accueil`);
            } else {
                res.send("mot de passe invalide")
            }
        } else {
            console.log("page introuvable !!")
            
            res.send("page introuvable !!")
        }
    });
});
//////////////////creation///////////////////////

app.post('/validation/creation', function (req, res) {
    let loginuser = req.body.pseudo
    let mdpuser = req.body.mdp
    let titre = req.body.titre
    let post = req.body.post
    con.query(` SELECT * FROM User WHERE username ="${loginuser}";`, (err, result, fields) => {
        let idpost = result[0].idPost
        
        let mdpverif = bcrypt.compareSync(mdpuser, result[0].password)
        if (mdpverif == true) {
            con.query(`INSERT INTO Post (Titre, Texte, Utilisateur_idPost) VALUES ("${titre}", "${post}",${idpost});`);
            res.redirect("/accueil")
            
        } else {
            res.send("Mot de passe incorect")
        }
    });
    
});



////////////////////// afficher titre poste accueil///////////////////////////////////

app.get("/afficherpost", function (req, res) {
    con.query(`SELECT * FROM Blog.Post;`, (err, result, fields) => {
        res.json(result);
    })
});


////////////////////////afficher list user///////////////////////////////

app.get("/manipulation/afficherlist/:login/:mdp", function (req, res) {
    loginuser = req.params.login;
    mdpuser = req.params.mdp;
    con.query(` SELECT * FROM User WHERE username ="${loginuser}";`, (err, result, fields) => {
        if (result.length < 1) {
            res.json({
                error : "login non exitente"
            });
        } else if (loginuser === result[0].username) {
            iduser = result[0].idPost;
            let mdpverif = bcrypt.compareSync(mdpuser, result[0].password)
            if (mdpverif == true) {
                con.query(`SELECT Titre,Texte,idPost FROM Blog.Post WHERE Utilisateur_idPost = "${iduser}";`, (err, result, fields) => {
                    res.json(result)
                });

            } else {
                res.json({
                    error : "mot de passe invalide"
                })
            }
        }
    });
});

//////////////////page de connexion/////////////////

app.get("/", function (req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.sendFile('index.html');
});

///////////////page de formulaire/////////////////

app.get("/inscription", function (req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.sendFile(__dirname + '/public/formulaire_inscription.html');
});


///////////////page de formulaire echec//////////////////

app.get("/inscription/echec", function (req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.sendFile(__dirname + '/public/formulaire_inscription_echec.html');

});


///////////////////accueil///////////////////////////

app.get("/accueil", function (req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.sendfile(__dirname + '/public/accueil.html');
});

//////////////////liste post////////////////////

app.get("/liste_post", function (req, res) {
    res.sendFile(__dirname + '/public/list_post.html');
});

/////////////////creation post///////////////////////

app.get("/creation_post", function (req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.sendFile(__dirname + '/public/creation_post.html');
});

/////////////////modif/supp post//////////////////////////

app.get("/modif_supp_post", function (req, res) {
    res.sendFile(__dirname + '/public/modif_supp_post.html');
});




app.listen(port, () => console.log(`Je suis lancé ! ${port}`));




// const { Random } = require("random-js");
// const random = new Random();
// const value = random.integer(1, 10);
// console.log("Chut... voici le nombre mystère : " + value);
// console.log(__dirname)
// app.use(express.static('public'));
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());

// app.get("/", function(req, res) {
//     res.sendFile("index.html"); // ou sendFile
// })

// app.get("/nouvelle_chance_trop_haut/", function(req, res) {
//     res.sendFile(__dirname + '/public/nouvelle_chance_trop_haut.html');
// })
// app.get("/nouvelle_chance_trop_bas/", function(req, res) {
//     res.sendFile(__dirname + '/public/nouvelle_chance_trop_bas.html');
// })


// app.post("/reponse/", function(req, res) {
//     console.log(`le chiffre ${req.body.chiffre}`)
//     var reponse = req.body.chiffre;
//     if (reponse == value) {
//         console.log("Gagné !");
//         res.send("Gagné");
//     } else if (reponse > value) {
//         console.log("Perdu trop haut!");
//         res.redirect("/nouvelle_chance_trop_haut/");
//     } else {
//         console.log("Perdu trop bas!");
//         res.redirect("/nouvelle_chance_trop_bas/");
//     }
// })